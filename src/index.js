import React from 'react';
import ReactDOM from 'react-dom';
import HeaderBlock from "./components/HeaderBlock";
import './index.css';

// const el = React.createElement(
//     'h1', null, 'Hello World. React.js'
// );

const AppHeader = () => {
    const margin = 40;
    // const  headerStyle = {
    //     color: 'red',
    //     marginLeft: `${margin}px`,
    //     marginBottom: `${margin}px`
    // };

    return (
        <h1 className="header">Hellow World</h1>
    );
}

const AppInput = () => {
    const placeholder = 'Type text...';
    return (
        <label htmlFor="search">
            <input id="search" placeholder={placeholder} />
        </label>
    )
}

const AppList = () => {
    const items = ['Item 1', "Item 2", "Item 3", "Item 4"];
    const firstItem = <li>Item 0</li>;

    const isAuth = false;

    return (
        <ul>
            { isAuth ? firstItem : null }
            { items.map(item => <li>{item}</li>) }
            <li>{ items[0] }</li>
            <li>{ items[1] }</li>
        </ul>
    );
}

const App = () => {
    return (
        <>
            <HeaderBlock/>
        </>
    );
}

ReactDOM.render(<App />, document.getElementById('root'));